package com.test.poc.service;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RoomServiceTest {

    private final RoomService service = new RoomService();

    @Test
    public void testFindRoomsByExams() {
        assertEquals(2, service.findRoomsByExams(Set.of("USG de Mamas")).size());
        assertEquals(1, service.findRoomsByExams(Set.of("USG de Mamas",  "USG de Tireóide")).size());
        assertEquals(1, service.findRoomsByExams(Set.of("Vacina da Gripe", "Vacina de HPV")).size());
        assertEquals(2, service.findRoomsByExams(Set.of("Mamografia")).size());
    }
}