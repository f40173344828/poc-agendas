package com.test.poc.service;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class RuleServiceTest {

    private final RuleService ruleService = new RuleService();

    @Test
    public void testApplyExamGroupingRule() {
        assertEquals(-10, ruleService.applyExamGroupingRule(Set.of("RM de Ombro direito", "RM de Ombro esquerdo")));
        assertEquals(0, ruleService.applyExamGroupingRule(Set.of("RM de Ombro direito", "Mamografia")));
        assertEquals(5, ruleService.applyExamGroupingRule(Set.of("USG de Mamas", "USG de Tireóide")));
        assertEquals(0, ruleService.applyExamGroupingRule(Set.of()));
    }

    @Test
    public void testApplyRoomExtensionRule() {
        assertTrue(ruleService.applyRoomExtensionRule(Set.of(1), 1).isEmpty());
        assertTrue(ruleService.applyRoomExtensionRule(Set.of(), 1).isEmpty());
        assertEquals(Map.of(5, -15), ruleService.applyRoomExtensionRule(Set.of(5), 2));
    }

}