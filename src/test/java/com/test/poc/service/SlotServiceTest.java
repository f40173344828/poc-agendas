package com.test.poc.service;

import com.test.poc.domain.entity.Exam;
import com.test.poc.domain.entity.Room;
import com.test.poc.domain.entity.Slot;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class SlotServiceTest {

    @Mock
    private RuleService ruleService;

    @InjectMocks
    private SlotService slotService;

    @Test
    public void testCalculateSlotTime() {
        Exam examOne = Exam.builder().code("E1").time(15).build();
        Exam examTwo = Exam.builder().code("E2").time(10).build();

        Set<Exam> exams = Set.of(examOne, examTwo);
        Set<Integer> roomCodes = Set.of(1, 2);

        when(ruleService.applyExamGroupingRule(Set.of("E1", "E2"))).thenReturn(10);
        when(ruleService.applyRoomExtensionRule(roomCodes, 2)).thenReturn(Map.of(1, 10));

        Map<Integer, Integer> result = slotService.calculateSlotTime(exams, roomCodes);

        assertEquals(45, result.get(1));
        assertEquals(35, result.get(2));
    }

    @Test
    public void testCreateSlots() {
        Room roomOne = Room.builder()
                .code(1)
                .beginTime(LocalTime.of(8, 0 ,0))
                .endTime(LocalTime.of(10, 0, 0))
                .build();

        Room roomTwo = Room.builder()
                .code(2)
                .beginTime(LocalTime.of(9, 0 ,0))
                .endTime(LocalTime.of(12, 0, 0))
                .build();

        Set<Room> rooms = Set.of(roomOne, roomTwo);
        Map<Integer, Integer> slotTimeByRoom = Map.of(1, 15, 2, 70);

        Map<Integer, List<Slot>> slots = slotService.createSlots(rooms, slotTimeByRoom);

        assertEquals(8, slots.get(1).size());
        assertEquals(2, slots.get(2).size());
    }
}