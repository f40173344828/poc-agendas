package com.test.poc.service;

import com.test.poc.controller.response.FindScheduleResponse;
import com.test.poc.domain.entity.Exam;
import com.test.poc.domain.entity.Room;
import com.test.poc.domain.entity.Schedule;
import com.test.poc.domain.repository.ScheduleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ScheduleServiceTest {

    @Mock
    private RoomService roomService;

    @Mock
    private ExamService examService;

    @Mock
    private SlotService slotService;

    @InjectMocks
    private ScheduleService scheduleService;

    @Test
    public void  testFindSchedule_with_schedule() {
        Set<String> examsCodes = Set.of("USG de Mamas", "USG de Tireóide");
        String gender = "F";

        Room room = Room.builder()
                .code(1)
                .exams(Set.of("USG de Mamas", "USG de Tireóide"))
                .beginTime(LocalTime.of(8, 0, 0))
                .endTime(LocalTime.of(18, 0, 0))
                .limit(10)
                .build();

        when(roomService.findRoomsByExams(examsCodes)).thenReturn(Set.of(room));

        when(examService.findExamByCode("USG de Mamas"))
                .thenReturn(Optional.of(new Exam("USG de Mamas", Set.of(1, 2 ,3), 10, "F")));

        when(examService.findExamByCode("USG de Tireóide"))
                .thenReturn(Optional.of(new Exam("USG de Tireóide", Set.of(1), 10, "U")));

        when(slotService.calculateSlotTime(any(), any())).thenReturn(Map.of(1, 10));

        ScheduleRepository.addSchedule(Schedule.builder()
                .roomCode(1)
                .examCode("USG de Tireóide")
                .beginTime(LocalTime.parse("09:30:00"))
                .endTime(LocalTime.parse("10:30:00"))
                .build());

        ScheduleRepository.addSchedule(Schedule.builder()
                .roomCode(1)
                .examCode("USG de Tireóide")
                .beginTime(LocalTime.parse("11:30:00"))
                .endTime(LocalTime.parse("12:30:00"))
                .build());

        when(slotService.createSlots(any(), any())).thenCallRealMethod();

        Set<FindScheduleResponse> result = scheduleService.findSchedule(examsCodes, gender);

        assertEquals(1, result.size());
        result.stream().findFirst()
                .ifPresentOrElse(response -> {
                    assertEquals(1, response.getRoomCode());
                    assertEquals(48, response.getSlots().size());
                }, Assertions::fail);
    }

    @Test
    public void  testFindSchedule_no_schedule() {
        Set<String> examsCodes = Set.of("USG de Mamas", "USG de Tireóide");
        String gender = "F";

        Room room = Room.builder()
                .code(1)
                .exams(Set.of("USG de Mamas", "USG de Tireóide"))
                .beginTime(LocalTime.of(8, 0, 0))
                .endTime(LocalTime.of(18, 0, 0))
                .limit(10)
                .build();

        when(roomService.findRoomsByExams(examsCodes)).thenReturn(Set.of(room));

        when(examService.findExamByCode("USG de Mamas"))
                .thenReturn(Optional.of(new Exam("USG de Mamas", Set.of(1, 2 ,3), 10, "F")));

        when(examService.findExamByCode("USG de Tireóide"))
                .thenReturn(Optional.of(new Exam("USG de Tireóide", Set.of(1), 10, "U")));

        when(slotService.calculateSlotTime(any(), any())).thenReturn(Map.of(1, 10));

        when(slotService.createSlots(any(), any())).thenCallRealMethod();

        Set<FindScheduleResponse> result = scheduleService.findSchedule(examsCodes, gender);

        assertEquals(1, result.size());
        result.stream().findFirst()
                .ifPresentOrElse(response -> {
                    assertEquals(1, response.getRoomCode());
                    assertEquals(60, response.getSlots().size());
                }, Assertions::fail);
    }
}