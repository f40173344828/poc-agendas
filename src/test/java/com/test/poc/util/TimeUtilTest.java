package com.test.poc.util;

import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static com.test.poc.util.TimeUtil.isTimeConflicting;
import static org.junit.jupiter.api.Assertions.*;

class TimeUtilTest {

    @Test
    public void testCheckTimeConflict() {
        assertTrue(isTimeConflicting(LocalTime.parse("12:00:00"), LocalTime.parse("13:00:00"),
                LocalTime.parse("12:00:00"), LocalTime.parse("14:00:00")));

        assertTrue(isTimeConflicting(LocalTime.parse("11:00:00"), LocalTime.parse("14:00:00"),
                LocalTime.parse("12:00:00"), LocalTime.parse("13:00:00")));

        assertTrue(isTimeConflicting(LocalTime.parse("12:00:00"), LocalTime.parse("13:00:00"),
                LocalTime.parse("10:00:00"), LocalTime.parse("14:00:00")));

        assertTrue(isTimeConflicting(LocalTime.parse("12:00:00"), LocalTime.parse("13:00:00"),
                LocalTime.parse("12:00:00"), LocalTime.parse("13:00:00")));

        assertTrue(isTimeConflicting(LocalTime.parse("12:00:00"), LocalTime.parse("13:00:00"),
                LocalTime.parse("09:00:00"), LocalTime.parse("13:00:00")));

        assertFalse(isTimeConflicting(LocalTime.parse("12:00:00"), LocalTime.parse("13:00:00"),
                LocalTime.parse("13:00:00"), LocalTime.parse("14:00:00")));

        assertFalse(isTimeConflicting(LocalTime.parse("10:00:00"), LocalTime.parse("13:00:00"),
                LocalTime.parse("13:00:00"), LocalTime.parse("14:00:00")));

        assertFalse(isTimeConflicting(LocalTime.parse("12:00:00"), LocalTime.parse("13:00:00"),
                LocalTime.parse("18:00:00"), LocalTime.parse("20:00:00")));
    }
}