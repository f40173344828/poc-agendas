package com.test.poc.domain.entity;

import lombok.Data;

import java.util.Set;

@Data
public class GroupingExtensionRule {

    private final Set<String> examCodes;

    private final int adjustmentTime;
}
