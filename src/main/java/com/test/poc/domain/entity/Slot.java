package com.test.poc.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;

@Data
@Builder
@AllArgsConstructor
public class Slot {

    private int roomCode;

    private LocalTime beginTime;

    private LocalTime endTime;

}
