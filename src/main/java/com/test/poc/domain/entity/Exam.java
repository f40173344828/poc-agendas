package com.test.poc.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class Exam {

    private String code;

    private Set<Integer> rooms;

    private Integer time;

    private String gender;
}
