package com.test.poc.domain.entity;

import lombok.Data;

import java.util.Set;

@Data
public class RoomExtensionRule {

    private final int roomCode;

    private final int examQuantity;

    private final int adjustmentTime;
}
