package com.test.poc.domain.entity;

import lombok.*;

import java.time.LocalTime;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
public class Room {

    private Integer code;

    private Set<String> exams;

    private LocalTime beginTime;

    private LocalTime endTime;

    private Integer limit;

}
