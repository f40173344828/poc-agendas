package com.test.poc.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;

@Data
@Builder
@AllArgsConstructor
public class Schedule {

    private Integer roomCode;

    private String examCode;

    private LocalTime beginTime;

    private LocalTime endTime;
}
