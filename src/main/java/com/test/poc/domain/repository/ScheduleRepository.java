package com.test.poc.domain.repository;

import com.test.poc.domain.entity.Schedule;

import java.util.*;
import java.util.stream.Collectors;

public class ScheduleRepository {

    private static final Map<Integer, Set<Schedule>> schedulesByRoom = new HashMap<>();

    public static Set<Schedule> findAllSchedulesByRoom() {
        return schedulesByRoom.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    public static Map<Integer, Set<Schedule>> findSchedulesByRoom(Set<Integer> roomCodes) {
        return roomCodes.stream()
                .map(roomCode -> Map.entry(roomCode, schedulesByRoom.getOrDefault(roomCode, Set.of())))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public static Set<Schedule> findSchedulesByRoom(Integer roomCode) {
        return Optional.ofNullable(schedulesByRoom.get(roomCode))
                .orElseGet(HashSet::new);
    }

    public static void addSchedule(Schedule schedule) {
        if (schedulesByRoom.containsKey(schedule.getRoomCode())) {
            schedulesByRoom.get(schedule.getRoomCode()).add(schedule);
        } else {
            Set<Schedule> schedules = new HashSet<>();
            schedules.add(schedule);
            schedulesByRoom.put(schedule.getRoomCode(), schedules);
        }
    }
}
