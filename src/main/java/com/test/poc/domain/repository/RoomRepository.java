package com.test.poc.domain.repository;

import com.test.poc.domain.entity.Room;

import java.time.LocalTime;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static java.time.LocalTime.parse;

public class RoomRepository {

    private static final Map<Integer, Room> rooms = Map.ofEntries(
        Map.entry(1, new Room(1, Set.of("USG de Mamas", "USG de Tireóide"), parse("08:00:00"), parse("18:00:00"), 10)),
        Map.entry(2, new Room(2, Set.of("Mamografia"), parse("08:00:00"), parse("16:00:00"), 100)),
        Map.entry(3, new Room(3, Set.of("Mamografia", "USG de Mamas"), parse("08:00:00"), parse("15:00:00"), 4)),
        Map.entry(4, new Room(4, Set.of("Teste Ergométrico", "Holter"), parse("08:00:00"), parse("18:00:00"), 5)),
        Map.entry(5, new Room(5, Set.of("Vacina da Gripe", "Vacina de HPV", "Vacina de Tétano"), parse("08:00:00"), parse("21:00:00"), 100)),
        Map.entry(6, new Room(6, Set.of("RM de Ombro direito", "RM de Ombro esquerdo"), parse("08:00:00"), parse("14:00:00"), 999))
    );

    public static Set<Room> getRooms() {
        return new HashSet<>(rooms.values());
    }

    public static Optional<Room> findRoomByCode(Integer code) {
        return Optional.ofNullable(rooms.get(code));
    }
}
