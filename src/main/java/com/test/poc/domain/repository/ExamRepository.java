package com.test.poc.domain.repository;

import com.test.poc.domain.entity.Exam;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class ExamRepository {

    private static final Map<String, Exam> exams = Map.ofEntries(
        Map.entry("USG de Mamas", new Exam("USG de Mamas", Set.of(1, 2 ,3), 10, "F")),
        Map.entry("USG de Tireóide", new Exam("USG de Tireóide", Set.of(1), 10, "U")),
        Map.entry("Mamografia", new Exam("Mamografia", Set.of(2, 3), 8, "F")),
        Map.entry("Vacina da Gripe", new Exam("Vacina da Gripe", Set.of(5), 15, "U")),
        Map.entry("Vacina de HPV", new Exam("Vacina de HPV", Set.of(5), 15, "U")),
        Map.entry("Vacina de Tétano", new Exam("Vacina de Tétano", Set.of(5), 15, "U")),
        Map.entry("RM de Ombro direito", new Exam("RM de Ombro direito", Set.of(6), 10, "U")),
        Map.entry("RM de Ombro esquerdo", new Exam("RM de Ombro esquerdo", Set.of(6), 10, "U")),
        Map.entry("Holter", new Exam("Holter", Set.of(4), 10, "U")),
        Map.entry("Teste Ergométrico", new Exam("Teste Ergométrico", Set.of(4), 30, "U"))
    );

    public static Set<Exam> getExams() {
        return new HashSet<>(exams.values());
    }

    public static Optional<Exam> findRoomByCode(String code) {
        return Optional.ofNullable(exams.get(code));
    }
}
