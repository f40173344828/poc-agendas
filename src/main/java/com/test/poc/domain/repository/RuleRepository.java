package com.test.poc.domain.repository;

import com.test.poc.domain.entity.GroupingExtensionRule;
import com.test.poc.domain.entity.RoomExtensionRule;

import java.util.Set;

public class RuleRepository {

    private static final Set<GroupingExtensionRule> GROUPING_EXTENSION_RULES = Set.of(
            new GroupingExtensionRule(Set.of("RM de Ombro direito", "RM de Ombro esquerdo"), -10),
            new GroupingExtensionRule(Set.of("USG de Mamas", "USG de Tireóide"), 5)
    );

    private static final Set<RoomExtensionRule> ROOM_EXTENSION_RULES = Set.of(
            new RoomExtensionRule(5, 2, -15),
            new RoomExtensionRule(5, 3, -20),
            new RoomExtensionRule(5, 4, -25),
            new RoomExtensionRule(5, 5, -30)
    );

    public static Set<GroupingExtensionRule> getGroupingExtensionRules() {
        return GROUPING_EXTENSION_RULES;
    }

    public static Set<RoomExtensionRule> getRoomExtensionRules() {
        return ROOM_EXTENSION_RULES;
    }
}
