package com.test.poc.controller;

import com.test.poc.controller.request.CreateScheduleRequest;
import com.test.poc.controller.response.FindScheduleResponse;
import com.test.poc.service.ScheduleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@Slf4j
@RestController
public class ScheduleController {

    private final ScheduleService scheduleService;

    public ScheduleController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    @GetMapping("/api/schedule/find")
    public Set<FindScheduleResponse> findScheduleResponse(
            @RequestParam Set<String> exams,
            @RequestParam String gender) {
        log.info("m=searchScheduleResponse");
        return scheduleService.findSchedule(exams, gender);
    }

    @PostMapping("/api/schedule/create")
    public ResponseEntity<Object> createSchedule(@RequestBody CreateScheduleRequest request) {
        log.info("m=searchScheduleResponse");
        scheduleService.createSchedule(request);

        return ResponseEntity.status(201).build();
    }
}
