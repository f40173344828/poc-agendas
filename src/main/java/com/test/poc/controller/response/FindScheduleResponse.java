package com.test.poc.controller.response;

import com.test.poc.domain.entity.Slot;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FindScheduleResponse {

    private int roomCode;

    private List<Slot> slots;
}
