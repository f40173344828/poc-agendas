package com.test.poc.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateScheduleRequest {

    @JsonProperty("room_code")
    private Integer roomCode;

    @JsonProperty("exam_code")
    private String examCode;

    @JsonProperty("begin_time")
    private LocalTime beginTime;

    @JsonProperty("end_time")
    private LocalTime endTime;
}
