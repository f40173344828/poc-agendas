package com.test.poc.util;

import java.time.LocalTime;

public class TimeUtil {

    public static boolean isTimeConflicting(LocalTime beginTimeA, LocalTime endTimeA, LocalTime beginTimeB, LocalTime endTimeB) {
        return beginTimeA.compareTo(endTimeB) < 0 && endTimeA.compareTo(beginTimeB) > 0;
    }
}
