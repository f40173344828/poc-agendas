package com.test.poc.service;

import com.test.poc.domain.entity.GroupingExtensionRule;
import com.test.poc.domain.entity.RoomExtensionRule;
import com.test.poc.domain.repository.RuleRepository;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RuleService {

    public int applyExamGroupingRule(Set<String> examCodes) {
        return RuleRepository.getGroupingExtensionRules()
                .stream()
                .filter(rule -> examCodes.containsAll(rule.getExamCodes()))
                .mapToInt(GroupingExtensionRule::getAdjustmentTime)
                .sum();
    }

    public Map<Integer, Integer> applyRoomExtensionRule(Set<Integer> roomCodes, int examQuantity) {
        return RuleRepository.getRoomExtensionRules()
                .stream()
                .filter(rule -> roomCodes.contains(rule.getRoomCode()) && examQuantity == rule.getExamQuantity())
                .collect(Collectors.toMap(RoomExtensionRule::getRoomCode, RoomExtensionRule::getAdjustmentTime));
    }
}
