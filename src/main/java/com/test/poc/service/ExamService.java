package com.test.poc.service;

import com.test.poc.domain.entity.Exam;
import com.test.poc.domain.repository.ExamRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class ExamService {

    public Optional<Exam> findExamByCode(String code) {
        log.info("m=findExamByCode");
        return ExamRepository.findRoomByCode(code);
    }
}
