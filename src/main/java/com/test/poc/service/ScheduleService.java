package com.test.poc.service;

import com.test.poc.controller.request.CreateScheduleRequest;
import com.test.poc.controller.response.FindScheduleResponse;
import com.test.poc.domain.entity.Exam;
import com.test.poc.domain.entity.Room;
import com.test.poc.domain.entity.Schedule;
import com.test.poc.domain.entity.Slot;
import com.test.poc.domain.repository.ScheduleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.test.poc.util.TimeUtil.isTimeConflicting;

@Slf4j
@Service
public class ScheduleService {

    private final RoomService roomService;

    private final ExamService examService;

    private final SlotService slotService;


    public ScheduleService(RoomService roomService, ExamService examService, SlotService slotService) {
        this.roomService = roomService;
        this.examService = examService;
        this.slotService = slotService;
    }

    public Set<FindScheduleResponse> findSchedule(Set<String> examsCodes, String gender) {
        log.info("m=findSchedule, examsCodes={}, gender={}", examsCodes, gender);

        final Set<Room> rooms = roomService.findRoomsByExams(examsCodes);

        final Set<Exam> exams = examsCodes.parallelStream()
                .map(examService::findExamByCode)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(exam -> gender.equals(exam.getGender()))
                .collect(Collectors.toSet());

        if (exams.isEmpty()) return Set.of();

        final Set<Integer> roomCodes = rooms.parallelStream().map(Room::getCode).collect(Collectors.toSet());

        final Map<Integer, Integer> slotTimeByRoom = slotService.calculateSlotTime(exams, roomCodes);

        final Map<Integer, Set<Schedule>> schedulesByRoom = ScheduleRepository.findSchedulesByRoom(roomCodes);

        final Map<Integer, List<Slot>> slotsByRoom = slotService.createSlots(rooms, slotTimeByRoom);

        return slotsByRoom.entrySet().stream()
                .map(entry -> Map.entry(entry.getKey(), filterScheduledSlots(entry.getValue(), schedulesByRoom.get(entry.getKey()))))
                .map(entry -> FindScheduleResponse.builder().roomCode(entry.getKey()).slots(entry.getValue()).build())
                .collect(Collectors.toSet());
    }

    private List<Slot> filterScheduledSlots(List<Slot> slots, Set<Schedule> schedules) {
        return slots.stream()
                .filter(slot -> checkScheduleConflict(slot, schedules))
                .collect(Collectors.toList());
    }

    private boolean checkScheduleConflict(Slot slot, Set<Schedule> schedules) {
        return schedules.isEmpty() ||
                schedules.stream()
                        .noneMatch(schedule -> isTimeConflicting(
                                schedule.getBeginTime(),
                                schedule.getEndTime(),
                                slot.getBeginTime(),
                                slot.getEndTime()));
    }

    public void createSchedule(CreateScheduleRequest request) {
        log.info("m=createSchedule, request={}", request);

        Schedule schedule = Schedule.builder()
                .roomCode(request.getRoomCode())
                .examCode(request.getExamCode())
                .beginTime(request.getBeginTime())
                .endTime(request.getEndTime())
                .build();

        ScheduleRepository.addSchedule(schedule);
    }
}
