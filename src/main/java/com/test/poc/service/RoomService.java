package com.test.poc.service;

import com.test.poc.domain.entity.Room;
import com.test.poc.domain.repository.RoomRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RoomService {

    public Set<Room> findRoomsByExams(Set<String> examCodes) {
        log.info("m=findRoomsByExams");
        return RoomRepository.getRooms().stream()
                .filter(room -> room.getExams().containsAll(examCodes))
                .collect(Collectors.toSet());
    }
}
