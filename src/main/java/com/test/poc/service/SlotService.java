package com.test.poc.service;

import com.test.poc.domain.entity.Exam;
import com.test.poc.domain.entity.Room;
import com.test.poc.domain.entity.Slot;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SlotService {

    private final RuleService ruleService;

    public SlotService(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    public Map<Integer, Integer> calculateSlotTime(Set<Exam> exams, Set<Integer> roomCodes) {
        log.info("m=calculateSlotTime");

        final int plainDuration = exams.parallelStream()
                .mapToInt(Exam::getTime)
                .sum();

        final Set<String> examCodes = exams.parallelStream().map(Exam::getCode).collect(Collectors.toSet());

        final int timeAdjustment = ruleService.applyExamGroupingRule(examCodes);

        final Map<Integer, Integer> roomExtensionAdjustment = ruleService.applyRoomExtensionRule(roomCodes, exams.size());

        return roomCodes.stream()
                .map(roomCode -> Map.entry(roomCode, roomExtensionAdjustment.getOrDefault(roomCode, 0) + plainDuration + timeAdjustment))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public Map<Integer, List<Slot>> createSlots(Set<Room> rooms, Map<Integer, Integer> slotTimeByRoom) {
        log.info("m=createSlots");
        return rooms.parallelStream()
                .map(room -> Map.entry(room.getCode(), createSlots(room, slotTimeByRoom.get(room.getCode()))))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private List<Slot> createSlots(Room room, int slotTime) {
        log.info("m=createSlots, room={}, slotTime={}", room, slotTime);
        final List<Slot> roomSlots = new ArrayList<>();

        for (LocalTime start = room.getBeginTime(), end = room.getBeginTime().plusMinutes(slotTime);
             end.isBefore(room.getEndTime()) || end.equals(room.getEndTime());
             start = end, end = end.plusMinutes(slotTime)) {

            Slot slot = Slot.builder()
                    .roomCode(room.getCode())
                    .beginTime(start)
                    .endTime(end)
                    .build();

            roomSlots.add(slot);
        }

        return roomSlots;
    }
}
